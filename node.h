#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node{
	int data;
	struct node* next;
};

typedef struct node* ll;

ll init();

int add(ll,int);

void print(ll);

int delete(ll,int);

int search(ll,int);

void releaseNode(ll);

ll getNode();

void freeNodes(ll);

