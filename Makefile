COMPILEFLAGS=-g
CCOMP=gcc

prog: main.o init.o add.o print.o delete.o search.o getNode.o releaseNode.o freeNodes.o node.h
	$(CCOMP) $(COMPILEFLAGS) -o prog main.o init.o add.o print.o delete.o search.o getNode.o releaseNode.o freeNodes.o
 
main.o: main.c add.c delete.c search.c print.c freeNodes.c
	$(CCOMP) $(COMPILEFLAGS) -c main.c init.c add.c delete.c search.c print.c freeNodes.c
 
delete.o: delete.c search.c releaseNode.c
	$(CCOMP) $(COMPILEFLAGS) -c delete.c search.c releaseNode.c
 
add.o: add.c search.c getNode.c
	$(CCOMP) $(COMPILEFLAGS) -c add.c search.c getNode.c
 
search.o: search.c 
	$(CCOMP) $(COMPILEFLAGS) -c search.c
 
print.o: print.c
	$(CCOMP) $(COMPILEFLAGS) -c print.c init.c
 
init.o: init.c
	$(CCOMP) $(COMPILEFLAGS) -c init.c
 
getNode.o: getNode.c
	$(CCOMP) $(COMPILEFLAGS) -c getNode.c

releaseNode.o: releaseNode.c
	$(CCOMP) $(COMPILEFLAGS) -c releaseNode.c

freeNode.o: freeNodes.c
	$(CCOMP) $(COMPILEFLAGS) -c freeNodes.c

clean: 
	rm main.o init.o add.o delete.o print.o search.o getNode.o releaseNode.o prog freeNodes.o 
