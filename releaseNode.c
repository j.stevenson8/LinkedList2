#include "node.h"

/*The releaseNode function frees the memory of a node that has been deleted.
 * It receives a pointer to the node that is to be freed.
 */

void releaseNode(ll node)
{
	free(node);
	return;
}
