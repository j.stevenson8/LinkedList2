#include "node.h"

/*The init function initializes the linked list by dynamically 
 * allocating free memory to create a sentinel node. It returns 
 * a pointer to the sentinel node of a linked list.
 */

ll init()
{
	ll new=malloc(sizeof(struct node));
	new->next=NULL;
	return new; //returns the pointer to a sentinel node of a linked list
}
	
