#include "node.h" 

/*The delete function deletes a node from the linked list. 
 * It receives a pointer to the sentinel node of the linked 
 * list and the number to be deleted. It returns a 1 if the 
 * node was deleted and a 0 if it was not found.
 */

int delete(ll node,int num)
{
	int result=search(node,num); //search the linked list for the number to be deleted
	ll prev; //prev is the node pointing to node

	if(result==0) //if the number was not found in the linked list, return a 0
	{
		return 0;
	}

	while(1) //loop until the node containing num is found
	{
		prev=node; //move forward in the list
		node=node->next;
		
		if(node->data==num) //if the current node contains the data value num,
				    //delete the node by making prev point to where node is pointing 
		{
			prev->next=node->next; 
			releaseNode(node); //free the memory given to node
			return 1;
		}
	}		
}
