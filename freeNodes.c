#include "node.h"

/*The function freeNodes frees up all memory allocated to the linked list during runtime.
 * It receives a pointer to the sentinel node of the linked list and returns nothing.
 */

void freeNodes(ll node)
{
	ll prev; //prev is the node pointing to node

	while(node->next!=NULL) //move through the list until you hit the last node
	{
		prev=node; //move one node forward
		node=node->next;
		free(prev); //free up the previous node
	}
	
	free(node); //free the last node
	return;
}		
