#include "node.h"

/*Add is a function that adds a new entry to the linked list. 
 * It receives a pointer to the sentinel node and the number 
 * to be added to the list. It returns a 1 if the node was 
 * successfully added and a 0 if it was not. 
 */

int add(ll node,int num)
{	
	int result=search(node,num); //search the linked list for the number to be added and save the return value in result
	ll add,prev; //creates two pointers to a struct node 
		     //add is a pointer to the node to be added
		     //prev is a pointer to the node pointing to node

	if(result==1) //if the number was found in the linked list, return a 0
	{
		return 0;
	}

	if(node->next==NULL) //if the sentinel node is the only node in the list,
			     //add the new node and make the setinel point to it
	{
		add=getNode(); //retrieves memory for a struct node and returns its pointer 
		add->next=node->next;
		node->next=add;
		add->data=num;
		return 1;
	}
		
	while(node->next!=NULL) //while the current node does not point to NULL...
	{
		prev=node; //move forward in the linked list
		node=node->next;

		if(node->data>num) //if the data value of the current node is greater than the number to be added,
				   //add the node between prev and node
		{
			add=getNode();
			add->next=prev->next;
			prev->next=add;
			add->data=num;
			return 1;
		}
	}
	add=getNode(); //if the numnber to be added is greater than any number in the list, 
		       //add the number to the end of the list
	node->next=add;
	add->data=num;
	add->next=NULL;
	return 1;
}	
