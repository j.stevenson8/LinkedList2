#include "node.h"

/*The print function prints out the contents of the linked list. 
 * It receives a pointer to the sentinel node of a linked list.
 */

void print(ll node)
{
	while(node->next!=NULL) //print the data stored in the linked list until the last node was printed
	{
		node=node->next;
		printf("%d ",node->data);
	}
	printf("\n");
	return;
}
