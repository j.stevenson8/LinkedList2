/*John Stevenson
 * CSE 222
 * 1-27-2020
 * This is the main program to PA2
 * This program creates a linked list and allows the user to add to and 
 * delete from the linked list, search for specific numbers in the linked 
 * list, and print out the contents of the linked list.
 *
 * The main function takes care of all user input. 
 * It calls functions based on user input and most of the programs output
 * to the screen.
 */

#include "node.h"

void main()
{
	ll sent; //sent is the sentinel node of the linked list
	
	sent=init(); //initialize the sentinel node

	char input[100],extra[100]; 
	char command;
	int num,result,flag; //flag is used to see if a command was executed during the while loop
	
	do
	{
		flag=0; 
		printf(">");
		fgets(input,100,stdin);
		result=sscanf(input,"%c%d%s",&command,&num,extra); //wait for user input and parse it
		
		if(result==1)
		{
			if(command=='p') //if the command typed was p, print the linked list 
			{
				flag=1;
				print(sent);
			}
			else if(command=='x') //if the command typed was x, free up all memory used and exit the program
			{
				flag=1;
				freeNodes(sent);
				return;
			}
		}
		else if(result==2)
		{
			if(command=='s') //if the command typed was s, search the linked list for the number following s
			{
				flag=1;
				result=search(sent,num);
				if(result==1)
				{
					printf("FOUND\n");
				}	
				else if(result==0)
				{
					printf("NOT FOUND\n");
				}
			}
			else if(command=='d') //if the command typed was d, delete the number after d from the linked list if it exists
			{
				flag=1;
				result=delete(sent,num);
				if(result==1)
				{
					printf("Success\n");
				}	
				else if(result==0)
				{
					printf("NODE NOT FOUND\n");
				}
			}
			else if(command=='i') //if the command typed was i, insert the number after i into the linked list if it does not already exist 
			{
				flag=1;
				result=add(sent,num);
				if(result==1)
				{
					printf("Success\n");
				}	
				else if(result==0)
				{
					printf("NODE ALREADY IN LIST\n");
				}
				else if(result==-1)
				{
					printf("OUT OF SPACE\n");
				}
			}
		}
		
		if(flag==0) //if none of the commands above were executed, print out a summary of all commands
		{
			printf("Unknown Command\nHere is a summary of valid commands:\n");	
			printf("\ti number\tinsert\n\t\tThe add command adds a number to the list\n\n");
			printf("\td number\tdelete\n\t\tThe delete command deletes a number from the list\n\n");
			printf("\ts number\tsearch\n\t\tThe search command searches the list for a number\n\n");
			printf("\tp\t\tprint\n\t\tThe print command prints out the list\n\n");
		}
	} while(1);
}

