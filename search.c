#include "node.h"

/*The search function searches the linked list for a specific data entry.
 * It receives a pointer to the sentinel node and the number for which to search.
 * It returns a 1 if the node was found and a 0 if it was not.
 */

int search(ll node,int num)
{
	while(node->next!=NULL) //look through the linked list until you hit the last node
	{
		node=node->next; //move forward in the linked list 
		
		if(node->data==num) //if the number was found, return a 1
		{
			return 1;
		}
	}
	
	return 0; //if the number was not found, return a 0
}
